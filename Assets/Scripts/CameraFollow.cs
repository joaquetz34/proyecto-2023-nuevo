using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;     // Objetivo a seguir
    public Transform leftBound, rightBound;
    private float leftBoundsWidth, rightBoundsWidth;     // calcular el ancho de nuestro l�mite

    public float smoothDampTime = 0.15f;     // para hacer que cuando mario se mueva la camara lo siga y no haga un cambio brusco, si no que vaya deslizando
    private Vector3 smoothDampVelocity = Vector3.zero;   // velocidad que va a tomar en el tiempo la camara
    private float camWidth, camHeight, levelMinX, levelMaxX;    // limites de la camara en minimos y maximos
    void Start()
    {
        camHeight = Camera.main.orthographicSize * 2;   //(camara principal.eltama�o)           // calculos de la camara de alto de la camara
        camWidth = camHeight * Camera.main.aspect;    // Anncho de la camara

        leftBoundsWidth = leftBound.GetComponentInChildren<SpriteRenderer>().bounds.size.x / 2;
        rightBoundsWidth = rightBound.GetComponentInChildren<SpriteRenderer>().bounds.size.x / 2;

        levelMinX = leftBound.position.x + leftBoundsWidth + (camWidth / 2);
        levelMaxX = rightBound.position.x - rightBoundsWidth - (camWidth / 2);
    }

    void Update()
    {
        float targetX = Mathf.Max(levelMinX, Mathf.Min(levelMaxX, target.position.x));
        float x = Mathf.SmoothDamp(transform.position.x, targetX, ref smoothDampVelocity.x, smoothDampTime);   // desplazamiento de la camara
        transform.position = new Vector3(x, transform.position.y, transform.position.z);
    }
}
